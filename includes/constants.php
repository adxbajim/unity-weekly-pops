<?php
	//Colours
	define('PURPLE', '#6A4F75');
	define('ORANGE', '#D97A27');
	define('GREEN', '#509643');
		
	// random stuff
	define("APP_NAME", "The Unity Platform");
	define("SITE_ROOT", "http://www.unityplatform.co.uk/"); 
	define("SOCKET_CLIENT_ID", "WEB"); 
	define("SOCKET_SERVER_ID", "CENTRAL"); 
	
	// process filters
	define("BASIC_STATS", "basic_stats");
	define("SUBMIT_COMMAND", "submit_command");
	define("COMMAND_REPLIES", "cmd_replies");
	define("DELETE_UNIT", "delete_unit");
	define("DISPLAY_TICKET", "display_ticket");
	define("DISPLAY_INVENTORY_TICKET", "display_inventory_ticket");
	define("DISPLAY_TICKET_POSTS", "display_ticket_posts");
	define("DISPLAY_INVENTORY_TICKET_POSTS", "display_inventory_ticket_posts");
	define("DISPLAY_CAMPAIGN", "display_campaign");
	define("GET_PERMISSIONS_EDITOR", "get_permissions_editor");
	define("LOGOUT", "logout");
	
	include 'setupPerms.php';
?>