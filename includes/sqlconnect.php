<?php
	include'sockets/SocketCommunicator.php';
	$apptitle = "Unity Platform";
	
	if(file_exists('includes/dbconfig.php')){
		require_once('includes/dbconfig.php');
	} else {
		require_once('dbconfig.php');
	}
	
	if (file_exists('includes/sqlconnection.php')){
		require_once('includes/sqlconnection.php');
	} else {
		require_once('sqlconnection.php');
	}
	
	//Create unity connection
	$unity_connection = new SQLConnection(DBHOST_1, DBUSER_1, DBPASS_1, DBNAME_1);
	
	//Refresh if $unity_connection has failed
	if ($unity_connection == null){
?>

	<script type="text/javascript">
		function redirect(){
			window.location.reload(true);
		}
		
		window.setTimeout(redirect, 5000);
	</script>
	
<?php
	}
	
	$qry = 'SELECT * FROM global_settings;';
	$result = $unity_connection->query($qry) or die(mysqli_error($unity_connection));
	$globalSettings = array();

	while ($setting = mysqli_fetch_assoc($result)){
		$globalSettings[$setting['key']] = $setting['value'];
	}
	unset($qry);
	unset($result);

	//Work out the time a unit must have posted in after to be considered online (set in global settings)
	$timeoutParts = explode(':', $globalSettings['unit_offline_timeout']);
	$timeoutSeconds = $timeoutParts[0] * 60 * 60;
	$timeoutSeconds += $timeoutParts[1] * 60;
	$timeoutSeconds += $timeoutParts[2];

	//Converted hours minutes seconds into just seconds, now take those seconds away from current time
	$unit_offline_time = time() - $timeoutSeconds;
	//And build a date from them
	$globalSettings['unit_offline_time'] = date('Y-m-d H:i:s', $unit_offline_time);
?>