<?php
class Message {
	private static $TO_FROM_LENGTH = 12;
	private static $TYPE_LENGTH = 20;
	private $from;
	private $to;
	private $expectingResponse;
	private $messageType;
	private $payload;
	
	/*
	 *	Create a message. Payloads must be added with a separate AddXXXPayload() 
     *	method after the message is created.
	 */
	function __construct($from, $to, $type, $expectingResponse=true){
		$this->from = substr($from, 0, self::$TO_FROM_LENGTH);
		$this->to = substr($to, 0, self::$TO_FROM_LENGTH);
		$this->expectingResponse = ($expectingResponse) ? 1 : 0;
		$this->messageType = substr($type, 0, self::$TYPE_LENGTH);
		
		$this->payload = array();
	}
	
	/*
	 *	Creates a message object from a byte stream. 
     *	Used to construct a message from a socket stream
	 */
	public static function FromBytes($byteStr){
		if (strlen($byteStr) <= 0) return false;
		
		for($i=0; $i < strlen($byteStr); $i++) {
			$buff[] = ord($byteStr[$i]);
		}
	
		$i = 0;
		//From
		$from = str_ireplace("\0", "", implode(array_map("chr", array_slice($buff, $i, self::$TO_FROM_LENGTH))));
		$i += self::$TO_FROM_LENGTH;

		//To
		$to = str_ireplace("\0", "", implode(array_map("chr", array_slice($buff, $i, self::$TO_FROM_LENGTH))));
		$i += self::$TO_FROM_LENGTH;

		//Expecting response
		$subArr = array_slice($buff, $i, 4);
		$val = unpack("N",pack("C*",$subArr[3],$subArr[2],$subArr[1],$subArr[0]));
		$expectingResponse = ($val == 1) ? true : false;
		$i += 4;

		//Message type
		$type = str_ireplace("\0", "", implode(array_map("chr", array_slice($buff, $i, self::$TYPE_LENGTH))));
		$i += self::$TYPE_LENGTH;

		//Payload
		$payload = str_ireplace("\0", "", array_slice($buff, $i));
		
		$message = new Message($from, $to, $type, $expectingResponse);
		$message->addBytePayload($payload);
		return $message;
	}
	
	/*
	 *	Converts a Message object to a byte array ready for transmitting across the network
	 */
	public function ToBytes(){
		$bytes = array();
		
		$bytes = array_merge($bytes, unpack('C*', str_pad($this->from, self::$TO_FROM_LENGTH, "\0")));
		$bytes = array_merge($bytes, unpack('C*', str_pad($this->to, self::$TO_FROM_LENGTH, "\0")));
		$bytes = array_merge($bytes, unpack('C*', pack("N", $this->expectingResponse)));
		$bytes = array_merge($bytes, unpack('C*', str_pad($this->messageType, self::$TYPE_LENGTH, "\0")));
		$bytes = array_merge($bytes, $this->payload);
		//Add ETX
		$bytes[] = 3;
		
		$byteStr = "";
		foreach ($bytes as $b){
			$byteStr .= chr($b);
		}
		return $byteStr;
	}
	
	/*
	 *	Who the message is from, this is one of:
     *	    • MAC address of the Unity Sharp unit
     *	    • "CENTRAL" for Unity Central
     *	    • "WEB" for Unity Web (currently not accepting messages)
     *	    • "GO" for a Unity GO unit
	 */
	public function From(){
		return $this->from;
	}
	
	/*
	 *	Who the message is meant for, this is one of:
     *	    • MAC address of the Unity Sharp unit
     *	    • "CENTRAL" for Unity Central
     *	    • "WEB" for Unity Web (currently not accepting messages)
     *	    • "GO" for a Unity GO unit
	 */
	public function To() {
		return $this->to;
	}
	
	/*
	 *	Whether the sender is expecting a response to their message. Allows platforms to know whether to discard messages after reading or whether to respond.
     *	Note: This is actually sent as a 32-bit int (0000 or 0001), hence 4 bytes.
	 */
	public function IsExpectingResponse() {
		return ($this->expectingResponse == 1) ? true : false;
	}
	
	/*
	 *	A string representing what the message is regarding. 
	 *	There is a large list of possible types, the current known ones are listed below.
	 */
	public function Type() {
		return $this->messageType;
	}
	
	/*
	 *	The message payload (e.g. the command to execute in a UNIT_COMMAND message, 
	 *	or a bitmap image when responding to a screenshot command).
	 */
	public function Payload(){
		return $this->payload;
	}
	
	/*
	 *	The message payload as an ASCII string. Whether it was meant to be or not.
	 */
	public function PayloadAsString(){
		return implode(array_map("chr", $this->payload));
	}
	
	/*
	 *	Add a byte payload. Allows for raw byte payloads to be added, meaning any data format can be represented
	 */
	public function AddBytePayload($payload){
		if (is_array($payload)){			
			$this->payload = $payload;
		} else {
			echo 'Err: Payload must be a byte array';
			return false;
		}
		
		return true;
	}
	
	/*
	 *	Utility method that converts a string into a byte array and then calls AddBytePayload with that byte array
	 */
	public function AddStringPayload($message){
		$messageBytes = array();
		
		for($i = 0; $i < strlen($message); $i++) {
		   $messageBytes[$i] = ord($message[$i]);
		}
		
		return $this->addBytePayload($messageBytes);
	}
}
?>