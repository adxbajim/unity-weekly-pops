<?php
	include 'Message.php';

	/*
	 * Responsible for sending messages to the central server, which
	 * then redirects the command to the relevant unit
	 */
	class SocketCommunicator {
		private static $INSTANCE;
		const SERVER_PORT = 7000;
		const SEND_ATTEMPTS = 3;
		const UNIT_TYPE = 103;	//Unity Web
		
		private $clientId = SOCKET_CLIENT_ID;
		private $socket;
		private $latestErrNo;
		private $latestErrStr;

		public static function get() {
			if (!isset($INSTANCE) || $INSTANCE != null){
				$INSTANCE = new SocketCommunicator();
			}
			
			//Generate a unique id for this client
			$INSTANCE->clientId = strtoupper(substr(uniqid(SOCKET_CLIENT_ID), 0, 12));
			
			return $INSTANCE;
		}
		
		//Private functions
		private function openConnection($ip){
			$this->socket = stream_socket_client("tcp://".$ip.":".self::SERVER_PORT, $this->latestErrNo , $this->latestErrStr);
			//$this->socket = @fsockopen ($ip, self::SERVER_PORT, $this->latestErrNo , $this->latestErrStr);
			
			//Check if connect worked
			if ($this->socket === false) {
				return false;
			}
			
			//Listen for HELLO PING
			$helloMessage = $this->readMessage();
			if ($helloMessage === false){
				$this->socket = false;
				$this->latestErrStr = 'Timed out waiting for hello from Central';
				return false;
			} else {
				if ($helloMessage->Type() == 'PING'){
					return true;
				} else {
					$this->socket = false;
					$this->latestErrStr = 'Unexpected response received from Central ('.$helloMessage->Type().')';
					return false;
				}
			}
		}
		
		private function closeConnection(){
			@fclose($this->socket);
		}
		
		
		//Error functions
		public function errNo(){
			return $this->latestErrNo;
		}
		public function errStr(){
			return $this->latestErrStr;
		}
		public function err(){
			return "Socket error ".$this->errNo().': '.$this->errStr();
		}
		
		//Mesage send functions
		public function sendMessage($ip, $message){		
			if ($this->openConnection($ip) === false) {
				return false;
			}
			
			$connectMessage = new Message($this->clientId, SOCKET_SERVER_ID, 'CONNECT', false);
			
			session_start();
			$messageBytes = array();
			$messageBytes[] = self::UNIT_TYPE;
			for($i = 0; $i < strlen($_SESSION['name']); $i++) {
			   $messageBytes[] = ord($_SESSION['name'][$i]);
			}
			
			echo 'Command to '.$message->To().' via '.$ip.'<br/><br/>';
			
			$connectMessage->AddBytePayload($messageBytes);
			if (!$this->doSend($ip, $connectMessage)) return false;
			
			//Adjust message to use the correct from field regardless of what the caller set
			$adjustedMessage = new Message($this->clientId, $message->To(), $message->Type(), $message->IsExpectingResponse());
			$adjustedMessage->AddBytePayload($message->Payload());
			
			$return = $this->doSend($ip, $adjustedMessage);
			
			$disconnectMessage = new Message($this->clientId, SOCKET_SERVER_ID, 'DISCONNECT', false);
			if (!$this->doSend($ip, $disconnectMessage)) return false;
			
			$this->closeConnection();
			
			return $return;
		}
		
		private function doSend($ip, $message){
			$attempts = 0;
			$outOfAttempts = false;
			$messageBytes = $message->ToBytes();		
			
			while ($this->fwrite_stream($this->socket, $messageBytes) < strlen($messageBytes)){
				if ($attempts < self::SEND_ATTEMPTS){
					$outOfAttempts = true;
					break;
				}
			
				$this->openConnection($ip);
				$attempts++;
			}
			
			if ($outOfAttempts){
				$this->latestErrNo = -1;
				$this->lastestErrStr = 'Out of attempts';
				return false;
			} else {
				//We have sent the message now wait for a response if needed
				if ($message->isExpectingResponse()){
					$response = $this->readMessage();
					return $response;
				} else {
					return true;
				}
			}
		}
		
		private function readMessage(){
			stream_set_timeout($this->socket, 10); //10 seconds read timeout
			$bufferSize = 16 * 1024;
			$response = "";
			
			//Get this once so we know how many bytes we have
			$stream_meta_data = stream_get_meta_data($this->socket);
			
			//Until we reach end of file
			while (!feof($this->socket)) {
				$breakOut = false;
				/*
				stream_get_contents() will attempt to read as many bytes as specified, if there aren't that many it will block until it
				either gets them or the timeout (10s) expires. Because of this we block until we read at least 49 bytes (smallest size a message
				can be), and then determine how many bytes are left on the stream. If there are more we read (16k or the amount left, whichever
				is smaller) bytes and repeat until finished.
				This avoids the 10s timeout at the end of this loop, and the call now only blocks on the initial wait for a message.
				*/
				$size = ($stream_meta_data['unread_bytes'] != 0) ? min($bufferSize, $stream_meta_data['unread_bytes']) : 49;
				//echo 'Reading '.$size.' bytes of response '.time().'<br/>';
				$data = stream_get_contents($this->socket, $size);
				//echo 'Read '.$size.' bytes of response '.time().'<br/>';
				
				
				if ($data === false) {
					$this->latestErrStr = "Timed out waiting for a response.";
					return false;
				} else {
					$index = strpos($data, chr(3));
					
					if ($index !== FALSE){
						$breakOut = true;
						$data = substr($data, 0, $index);
					}
					$response .= $data;
				}
				$stream_meta_data = stream_get_meta_data($this->socket);
				
				//If we have no EOF marker then break if there are no bytes left to read
				if($breakOut || $stream_meta_data['unread_bytes'] <= 0) {
					break;
				}
			}
			
			$response = Message::FromBytes($response);
			if ($response === false){
				$this->latestErrStr = "Timed out waiting for a response.";
			}
			
			return $response;
		}
		
		private function fwrite_stream($fp, $string) {
			for ($written = 0; $written < strlen($string); $written += $fwrite) {
				$fwrite = fwrite($fp, substr($string, $written));
				if ($fwrite === false) {
					return $written;
				}
			}
			return $written;
		}
	} 
?>